//
//  CalculatorApp.swift
//  Calculator
//
//  Created by ITBCA on 30/01/23.
//

import SwiftUI

@main
struct CalculatorApp: App {
    var body: some Scene {
        WindowGroup {
            CalcView()
        }
    }
}
