//
//  CalculatorVM.swift
//  Calculator
//
//  Created by ITBCA on 02/02/23.
//

import Foundation


class CalculatorVM: ObservableObject {
    
    @Published var textToDisplay: String = "0"
    
    private var calcModel = Calculator()
    
    private var didEnterNumber: Bool = true
    
    private var lastClickedBtn = ""
    
    var calcBtns: [[String]] {
        return calcModel.btnArr
    }
    
    private var resultDisplayNum: Double {
        set {
            textToDisplay = "\(newValue)"
        }
        get {
            return Double(textToDisplay) ?? 0
        }
    }
    
    // isLastClickedOperator?
    func checkLastClickedBtnOperator() -> Bool {
        switch lastClickedBtn {
            case "+", "-", "x", "÷":
                return true
            default:
                return false
        }
    }
    
    func calcBtnClicked(btn: String) {
        switch btn {
            
            case "C", "+", "-", "x", "÷", "%", "+/-", "=":
                if checkLastClickedBtnOperator() {
                    break // so can't click + - x / continuously
                }
            
                didEnterNumber = true
                
                calcModel.setClickedNum(num: resultDisplayNum)
            
                if let res = calcModel.getCalculateResult(operand: btn) {
                    resultDisplayNum = res
                }
            
            
            case "⌫":
                
                didEnterNumber = true
                
                if textToDisplay != "0" && textToDisplay.count > 1 && Int(textToDisplay) != nil {
                        textToDisplay = "\(textToDisplay.dropLast())"
                    } else {
                        textToDisplay = "\(calcModel.clearCalcData())"
                        
                    }
            
        default:
            
            if didEnterNumber {
                
                if btn == "." {
                    textToDisplay += "."
                } else {
                    textToDisplay = btn
                }
                
                if resultDisplayNum == 0 && btn == "0" {
                    return
                }
                
                didEnterNumber = false
                
            } else {
                
                if btn == "." {
                    
                    // can't add more than one .
                    if textToDisplay.contains(".") {
                       return
                    }
                    
                    if Double(textToDisplay) == nil {
                        return
                    }
                    
                }
                textToDisplay += btn
            }
        }
        
        lastClickedBtn = btn
    }
    
    
}
