//
//  CalculatorModel.swift
//  Calculator
//
//  Created by ITBCA on 02/02/23.
//

import Foundation

struct Calculator {
    
    // btn to show
    var btnArr: [[String]] = [["C","⌫", "%", "÷"], ["7", "8", "9", "x"], ["4", "5", "6", "-"], ["1", "2", "3", "+"], ["+/-", "0", ".", "="]]
    
    private var clickedNumber: Double?
    var currentNum: Double?
    var prevNum: Double?
    var oprVal: String?
    var calcRes: Double?
    var clickedEqualBtnAgain = false
    
    
    mutating func setClickedNum(num: Double) {
        self.clickedNumber = num
    }
    
    mutating func getCalculateResult(operand: String) -> Double? {
        
        if clickedNumber != nil {
            
            switch operand {
                
                case "C":
                    return clearCalcData()
                    
                case "+/-":
                    return (clickedNumber != 0 ? (clickedNumber! * -1) : 0)
                    
                case "%":
                    clickedNumber = clickedNumber! / 100
                    return clickedNumber
                    
                case "=":
                    clickedEqualBtnAgain = true
                    
                    if prevNum != nil {
                        if currentNum != nil {
                            return calculateNum(numOne: prevNum!, numTwo: currentNum!)
                        } else {
                            currentNum = clickedNumber
                            return calculateNum(numOne: prevNum!, numTwo: clickedNumber!)
                            
                        }
                    }
                
            default:
                
                if clickedEqualBtnAgain {
                    clickedEqualBtnAgain = false
                    prevNum = nil
                }
                
                if prevNum != nil {
                    prevNum = calculateNum(numOne: prevNum!, numTwo: clickedNumber!)
                    oprVal = operand
                    return prevNum // so the calc get the temp result
                }
                
                oprVal = operand
                currentNum = nil
                prevNum = (prevNum == nil ? clickedNumber : prevNum)
                
            }
        }
        
        return nil
    }
    
    mutating func calculateNum(numOne: Double, numTwo: Double) -> Double? {
        
        if oprVal != nil {
            switch oprVal {
                case "+":
                    calcRes = numOne + numTwo
                case "-":
                    calcRes = numOne - numTwo
                case "÷":
                    calcRes = numOne / numTwo
                case "x":
                    calcRes = numOne * numTwo
                default:
                    break
                }
        }
        
        // to be able to calculate next operation from the current result
        self.prevNum = calcRes
        return calcRes
    }
    
    mutating func clearCalcData() -> Double {
        self.currentNum = nil
        self.prevNum = nil
        self.oprVal = nil
        return 0
    }
    
}

