//
//  ButtonView.swift
//  Calculator
//
//  Created by ITBCA on 30/01/23.
//

import Foundation
import SwiftUI

struct BtnNumView: View {
    var label: String
    var height: Int
    var width: Int
    
    var body: some View {
        Text(label)
            .font(.title2)
            .foregroundColor(.white)
            .frame(width:CGFloat(width), height: CGFloat(height), alignment: .center)
            .background(Int(label) == nil ? .orange : .gray)
            .cornerRadius(5.0)
    }
}
