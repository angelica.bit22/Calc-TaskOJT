//
//  CalcView.swift
//  Calculator
//
//  Created by ITBCA on 30/01/23.
//

import SwiftUI

struct CalcView: View {
    @ObservedObject var calcVM = CalculatorVM()
    
    // btn config
    let btnWidth = (Int(UIScreen.main.bounds.width) - 5 * 10) / 4
    let btnHeight = (Int(UIScreen.main.bounds.height) - 6 * 10) / 10
    
    
    var body: some View {
        VStack {
            Spacer()
            
            // text to display
            Text(calcVM.textToDisplay.count > 16 ? Double(calcVM.textToDisplay)!.scientificFormatted : "\(Double(Double(calcVM.textToDisplay)!.stringWithoutZeroFraction)!.withCommaSeparator())")
                .minimumScaleFactor(0.9)
                .font(.largeTitle)
                .frame(width: UIScreen.main.bounds.width - 20, height: CGFloat(btnHeight), alignment: .trailing)
                .padding(.horizontal, 10)
            
            // calc buttons
            ForEach(calcVM.calcBtns, id: \.self) { btnInRow in
                HStack {
                    ForEach(btnInRow, id: \.self) { btn in
                        Button {
                            if btn == "." {
                                if !calcVM.textToDisplay.contains(".") {
                                    calcVM.calcBtnClicked(btn: btn)
                                }
                            } else {
                                calcVM.calcBtnClicked(btn: btn)
                            }
                        } label: {
                            BtnNumView(label: btn, height: btnHeight, width: btnWidth)
                        }
                    }
                }
            }
        }
    }
}

struct CalcView_Previews: PreviewProvider {
    static var previews: some View {
        CalcView()
    }
}
